package com.project.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.project.dao.IuserLocal;
import com.project.dao.IuserRemote;
import com.project.modal.User;

@Stateless(name = "USER")
public class  UserEjbService implements IuserRemote,IuserLocal{
	
	@PersistenceContext
	private EntityManager em;
	

	@Override
	public void create(User t) {
		em.persist(t);
	}

	@Override
	public void delteById(User t) {
		em.remove(em.contains(t) ? t : em.merge(t));
		
	}

	@Override
	public User update(User t) {
		em.persist(em.contains(t) ? t : em.merge(t));
		return t;
	}

	@Override
	public User findById(User t) {
		
		return (User) em.find(User.class, t.getId());
	}

	@Override
	public List<User> findAll() {
		Query q = em.createQuery("select u from User u");
		List<User> users =  q.getResultList();
		System.out.println(users);
		return users;
	}
	

}
