package com.client;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.project.modal.Position;
import com.project.modal.Smartphone;
import com.project.modal.User;
import com.toedter.calendar.JCalendar;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class App extends JFrame {

	private JPanel contentPane;
	private JTextField nomtextField;
	private JTextField pnomtextField;
	private JTextField emailtextField;
	private JTextField teletextField;
	private JTextField imeitextField;
	private JTable table;

	private DefaultTableModel model;
	private JTable table2;
	private DefaultTableModel model2;
	private JTextField lattextField;
	private JTextField longtextField;
	private JTable table_1;
	private DefaultTableModel model3;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App frame = new App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public App() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 809, 499);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(22, 27, 763, 425);
		contentPane.add(tabbedPane);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.ORANGE);
		tabbedPane.addTab("Gestion phone", null, panel_1, null);
		panel_1.setLayout(null);

		JLabel lblNewLabel = new JLabel("Imei");
		lblNewLabel.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setBounds(125, 47, 45, 13);
		panel_1.add(lblNewLabel);

		imeitextField = new JTextField();
		imeitextField.setBounds(202, 43, 154, 19);
		panel_1.add(imeitextField);
		imeitextField.setColumns(10);

		JComboBox comboBox = new JComboBox();
		comboBox.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				User[] users = new User[EjbConnection.getrUser().findAll().size()];
				users = EjbConnection.getrUser().findAll().toArray(users);
				comboBox.removeAllItems();
				for (User u : users)
					comboBox.addItem(u);
			}
		});

		comboBox.setBounds(202, 73, 154, 21);
		panel_1.add(comboBox);

		JLabel lblNewLabel_1 = new JLabel("Utilisateur");
		lblNewLabel_1.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		lblNewLabel_1.setForeground(Color.BLUE);
		lblNewLabel_1.setBounds(122, 77, 70, 13);
		panel_1.add(lblNewLabel_1);

		

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(57, 173, 409, 175);
		panel_1.add(scrollPane_1);

		table2 = new JTable();
		model2 = new DefaultTableModel();
		Object[] column2 = { "ID", "IMEI", "NOM", "PRENOM" };
		Object[] rows2 = new Object[4];
		model2.setColumnIdentifiers(column2);
		table2.setModel(model2);
		scrollPane_1.setViewportView(table2);
		
		JButton btnNewButton_1 = new JButton("Ajouter");
		btnNewButton_1.setFont(new Font("Viner Hand ITC", Font.PLAIN, 11));
		btnNewButton_1.setBackground(Color.GREEN);
		btnNewButton_1.setForeground(Color.BLACK);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				User userphone = (User) comboBox.getSelectedItem();
				Smartphone s = new Smartphone(imeitextField.getText());
				s.setUser(userphone);
				EjbConnection.getrSphone().create(s);
				imeitextField.setText("");
				List<Smartphone> datas2 = EjbConnection.getrSphone().findAll();

				model2.setRowCount(0);

				for (Smartphone phone : datas2) {
					rows2[0] = phone.getId();
					rows2[1] = phone.getImei();
					rows2[2] = phone.getUser().getNom();
					rows2[3] = phone.getUser().getPrenom();

					model2.addRow(rows2);
				}
				JOptionPane.showMessageDialog(null, "Smartphone added");
			}
		});
		btnNewButton_1.setBounds(90, 122, 154, 21);
		panel_1.add(btnNewButton_1);

		List<Smartphone> datas2 = EjbConnection.getrSphone().findAll();

		model2.setRowCount(0);

		for (Smartphone phone : datas2) {
			rows2[0] = phone.getId();
			rows2[1] = phone.getImei();
			rows2[2] = phone.getUser().getNom();
			rows2[3] = phone.getUser().getPrenom();

			model2.addRow(rows2);
		}

		JButton btnNewButton_2 = new JButton("Delete");
		btnNewButton_2.setBackground(Color.RED);
		btnNewButton_2.setFont(new Font("Viner Hand ITC", Font.PLAIN, 11));
		btnNewButton_2.setForeground(Color.BLACK);
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = table2.getSelectedRow();
				Long id = (Long) table.getModel().getValueAt(i, 0);
				Smartphone u = new Smartphone();
				u.setId(id);
				EjbConnection.getrSphone().delteById(u);
				model2.removeRow(i);
				JOptionPane.showMessageDialog(null, "Smartphone deleted");

			}
		});
		btnNewButton_2.setBounds(272, 121, 154, 23);
		panel_1.add(btnNewButton_2);
		
		JLabel lblNewLabel_7 = new JLabel("");
		lblNewLabel_7.setIcon(new ImageIcon("C:\\Users\\x\\Downloads\\phone.jpg"));
		lblNewLabel_7.setBounds(33, 11, 59, 50);
		panel_1.add(lblNewLabel_7);
		
		
		
		model3 = new DefaultTableModel();
		Object[] column3 = { "ID", "latitude", "longitude", "date", "smartphone"};
		Object[] rows3 = new Object[5];
		model3.setColumnIdentifiers(column3);

		List<Position> datas3 = EjbConnection.getRposition().findAll();

		model3.setRowCount(0);

		for (Position p : datas3) {
			rows3[0] = p.getId();
			rows3[1] = p.getLatitude();
			rows3[2] = p.getLongitude();
			rows3[3] = p.getDate();
			rows3[4] = p.getSmartphone().getImei();
			model3.addRow(rows3);
		}

		JPanel panel = new JPanel();
		panel.setBackground(new Color(218, 112, 214));
		tabbedPane.addTab("Gestion User", null, panel, null);
		panel.setLayout(null);

		JLabel nomtextsss = new JLabel("Nom");
		nomtextsss.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		nomtextsss.setForeground(Color.BLUE);
		nomtextsss.setBounds(83, 36, 45, 13);
		panel.add(nomtextsss);

		JLabel pnomtextdddd = new JLabel("Prenom");
		pnomtextdddd.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		pnomtextdddd.setForeground(Color.BLUE);
		pnomtextdddd.setBounds(83, 66, 45, 13);
		panel.add(pnomtextdddd);

		nomtextField = new JTextField();
		nomtextField.setBounds(166, 33, 154, 19);
		panel.add(nomtextField);
		nomtextField.setColumns(10);

		pnomtextField = new JTextField();
		pnomtextField.setBounds(166, 62, 154, 19);
		panel.add(pnomtextField);
		pnomtextField.setColumns(10);

		JLabel Email = new JLabel("Email");
		Email.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		Email.setForeground(Color.BLUE);
		Email.setBounds(83, 96, 45, 13);
		panel.add(Email);

		emailtextField = new JTextField();
		emailtextField.setBounds(166, 92, 154, 19);
		panel.add(emailtextField);
		emailtextField.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("Tele");
		lblNewLabel_3.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		lblNewLabel_3.setForeground(Color.BLUE);
		lblNewLabel_3.setBounds(83, 284, 45, 13);
		panel.add(lblNewLabel_3);

		teletextField = new JTextField();
		teletextField.setBounds(166, 284, 154, 19);
		panel.add(teletextField);
		teletextField.setColumns(10);

		JCalendar calendar = new JCalendar();
		calendar.setBounds(166, 125, 198, 153);
		panel.add(calendar);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(374, 11, 364, 375);
		panel.add(scrollPane);

		table = new JTable();
		table.setBackground(new Color(176, 224, 230));
		model = new DefaultTableModel();
		Object[] column = { "ID", "NAME", "LNAME", "EMAIL", "DATEdeNaiss", "TELE" };
		Object[] rows = new Object[6];
		model.setColumnIdentifiers(column);
		table.setModel(model);
		scrollPane.setViewportView(table);

		List<User> datas = EjbConnection.getrUser().findAll();

		model.setRowCount(0);

		for (User user : datas) {
			rows[0] = user.getId();
			rows[1] = user.getNom();
			rows[2] = user.getPrenom();
			rows[3] = user.getEmail();
			rows[4] = user.getDateNaiss();
			rows[5] = user.getTelephone();
			model.addRow(rows);
		}

		JButton btnNewButton = new JButton("Ajouter");
		btnNewButton.setBackground(Color.GREEN);
		btnNewButton.setFont(new Font("Viner Hand ITC", Font.PLAIN, 11));
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				User u = new User(nomtextField.getText(), pnomtextField.getText(), teletextField.getText(),
						emailtextField.getText(), calendar.getDate());
				EjbConnection.getrUser().create(u);
				nomtextField.setText("");
				pnomtextField.setText("");
				teletextField.setText("");
				emailtextField.setText("");

				List<User> datas = EjbConnection.getrUser().findAll();

				model.setRowCount(0);

				for (User user : datas) {
					rows[0] = user.getId();
					rows[1] = user.getNom();
					rows[2] = user.getPrenom();
					rows[3] = user.getEmail();
					rows[4] = user.getDateNaiss();
					rows[5] = user.getTelephone();
					model.addRow(rows);
				}
				JOptionPane.showMessageDialog(null, "User a ete ajouter");

			}
		});
		btnNewButton.setBounds(40, 322, 128, 23);
		panel.add(btnNewButton);

		JLabel lblNewLabel_2 = new JLabel("Date de Naiss");
		lblNewLabel_2.setFont(new Font("Segoe Print", Font.PLAIN, 11));
		lblNewLabel_2.setForeground(Color.BLUE);
		lblNewLabel_2.setBounds(82, 125, 86, 14);
		panel.add(lblNewLabel_2);

		JButton btnNewButton_3 = new JButton("Delete");
		btnNewButton_3.setBackground(Color.RED);
		btnNewButton_3.setFont(new Font("Viner Hand ITC", Font.PLAIN, 11));
		btnNewButton_3.setForeground(Color.BLACK);
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				Long id = (Long) table.getModel().getValueAt(i, 0);
				User u = new User();
				u.setId(id);
				EjbConnection.getrUser().delteById(u);
				model.removeRow(i);

			}
		});
		btnNewButton_3.setBounds(192, 322, 128, 23);
		panel.add(btnNewButton_3);
		
		JLabel lblNewLabel_6 = new JLabel("");
		lblNewLabel_6.setIcon(new ImageIcon("C:\\Users\\x\\Downloads\\user.jpg"));
		lblNewLabel_6.setBounds(10, 12, 46, 50);
		panel.add(lblNewLabel_6);
		
				JPanel panel_2 = new JPanel();
				panel_2.setBackground(new Color(222, 184, 135));
				tabbedPane.addTab("Gestion Postion", null, panel_2, null);
				panel_2.setLayout(null);
				
				JLabel lblNewLabel_4 = new JLabel("Latitude");
				lblNewLabel_4.setFont(new Font("Segoe Print", Font.PLAIN, 11));
				lblNewLabel_4.setForeground(Color.BLUE);
				lblNewLabel_4.setBounds(90, 69, 46, 14);
				panel_2.add(lblNewLabel_4);
				
				JLabel lblNewLabel_4_1 = new JLabel("Longitude");
				lblNewLabel_4_1.setFont(new Font("Segoe Print", Font.PLAIN, 11));
				lblNewLabel_4_1.setForeground(Color.BLUE);
				lblNewLabel_4_1.setBounds(90, 94, 67, 14);
				panel_2.add(lblNewLabel_4_1);
				
				JLabel lblNewLabel_4_1_1 = new JLabel("Date");
				lblNewLabel_4_1_1.setFont(new Font("Segoe Print", Font.PLAIN, 11));
				lblNewLabel_4_1_1.setForeground(Color.BLUE);
				lblNewLabel_4_1_1.setBounds(72, 112, 32, 14);
				panel_2.add(lblNewLabel_4_1_1);
				
				JLabel lblNewLabel_4_1_1_1 = new JLabel("Smartphone");
				lblNewLabel_4_1_1_1.setFont(new Font("Segoe Print", Font.PLAIN, 11));
				lblNewLabel_4_1_1_1.setForeground(Color.BLUE);
				lblNewLabel_4_1_1_1.setBounds(22, 275, 78, 14);
				panel_2.add(lblNewLabel_4_1_1_1);
				
				lattextField = new JTextField();
				lattextField.setBounds(173, 66, 138, 20);
				panel_2.add(lattextField);
				lattextField.setColumns(10);
				
				longtextField = new JTextField();
				longtextField.setColumns(10);
				longtextField.setBounds(173, 91, 138, 20);
				panel_2.add(longtextField);
				
				JCalendar calendar_1 = new JCalendar();
				calendar_1.getYearChooser().getSpinner().setBackground(Color.ORANGE);
				calendar_1.getYearChooser().getSpinner().setForeground(Color.ORANGE);
				calendar_1.getMonthChooser().getComboBox().setForeground(Color.ORANGE);
				calendar_1.getDayChooser().getDayPanel().setForeground(Color.BLACK);
				calendar_1.setBounds(113, 111, 198, 153);
				panel_2.add(calendar_1);
				
				JComboBox comboBox_1 = new JComboBox();
				comboBox_1.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						Smartphone[] ss = new Smartphone[EjbConnection.getrSphone().findAll().size()];
						ss = EjbConnection.getrSphone().findAll().toArray(ss);
						comboBox_1.removeAllItems();
						for (Smartphone u : ss)
							comboBox_1.addItem(u);
					}
				});
				comboBox_1.setBounds(113, 271, 138, 22);
				panel_2.add(comboBox_1);
				
				JScrollPane scrollPane_2 = new JScrollPane();
				scrollPane_2.setBounds(370, 32, 274, 276);
				panel_2.add(scrollPane_2);
				
				table_1 = new JTable();
				table_1.setModel(model3);
				
				scrollPane_2.setViewportView(table_1);
				
						
						JButton btnNewButton_4 = new JButton("Ajouter");
						btnNewButton_4.setFont(new Font("Viner Hand ITC", Font.PLAIN, 11));
						btnNewButton_4.setBackground(Color.GREEN);
						btnNewButton_4.setForeground(Color.BLACK);
						btnNewButton_4.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
									
								comboBox_1.getSelectedItem();			
								Position p = new Position(lattextField.getText(),longtextField.getText(),calendar_1.getDate());
								p.setSmartphone((Smartphone)comboBox_1.getSelectedItem());
								EjbConnection.getRposition().create(p);
								
								List<Position> datas3 = EjbConnection.getRposition().findAll();

								model3.setRowCount(0);

								for (Position ps : datas3) {
									rows3[0] = ps.getId();
									rows3[1] = ps.getLatitude();
									rows3[2] = ps.getLongitude();
									rows3[3] = ps.getDate();
									rows3[4] = ps.getSmartphone().getImei();
									model3.addRow(rows3);
								}
								lattextField.setText("");longtextField.setText("");
								
								JOptionPane.showMessageDialog(null, "Postion added");
								
							}
						});
						btnNewButton_4.setBounds(22, 314, 126, 32);
						panel_2.add(btnNewButton_4);
						
						JButton btnNewButton_5 = new JButton("Delete");
						btnNewButton_5.setFont(new Font("Viner Hand ITC", Font.PLAIN, 11));
						btnNewButton_5.setBackground(Color.RED);
						btnNewButton_5.setForeground(Color.BLACK);
						btnNewButton_5.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								int i = table_1.getSelectedRow();
								Long id = (Long) table.getModel().getValueAt(i, 0);
								Position u = new Position();
								u.setId(id);
								EjbConnection.getRposition().delteById(u);
								model3.removeRow(i);
								JOptionPane.showMessageDialog(null, "Postion deleted");

							}
						});
						btnNewButton_5.setBounds(169, 314, 126, 32);
						panel_2.add(btnNewButton_5);
						
						JLabel lblNewLabel_5 = new JLabel("");
						lblNewLabel_5.setIcon(new ImageIcon("C:\\Users\\x\\Downloads\\pi.png"));
						lblNewLabel_5.setBounds(22, 11, 46, 58);
						panel_2.add(lblNewLabel_5);

	}
}
