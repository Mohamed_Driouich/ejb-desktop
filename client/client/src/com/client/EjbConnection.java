package com.client;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.project.dao.IphoneRemote;
import com.project.dao.IpositionRemote;
import com.project.dao.IuserRemote;


public class EjbConnection {
	private static IpositionRemote rposition;
	private static IphoneRemote rSphone;
	private static IuserRemote rUser;
	
	private static void connect(){
		Hashtable<Object, Object> jndiProperties = new Hashtable<Object, Object>();

		jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
		jndiProperties.put(Context.PROVIDER_URL, "http-remoting://localhost:8080");
		try {
		final Context context = new InitialContext(jndiProperties);

		rUser = (IuserRemote) context.lookup("ejb:/serveurEjb2/USER!com.project.dao.IuserRemote");
		rSphone = (IphoneRemote) context.lookup("ejb:/serveurEjb2/SPHONE!com.project.dao.IphoneRemote");
		rposition = (IpositionRemote) context.lookup("ejb:/serveurEjb2/POSITION!com.project.dao.IpositionRemote");
		}catch (Exception e) {
			System.out.println("probleme");
		}

	}

	public static IpositionRemote getRposition() {
		connect();
		return rposition;
	}

	public static IphoneRemote getrSphone() {
		connect();
		return rSphone;
	}

	public static IuserRemote getrUser() {
		connect();
		return rUser;
	}
	
	
	

	public static void main(String[] args) throws Exception {
		
	
		 
	}
}
